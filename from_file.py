# read in a text file with test headings
# '!' means notification/warning
# '?' means question with default yes-no answer with yes default is good
import sys
import re
from io import TextIOWrapper
from typing import List, Optional, Set
from pathlib import Path

from test import Test, parse_step

test_re = re.compile(r"^\w+-(\d{4})-T$")

def from_file(filename: str) -> List[Test]:
    global test_re
    state = "next_test"
    test_list: List[Test] = []
    current_test: Optional[Test] = None
    with open(filename, "r") as f:
        for raw_line in f.readlines():
            line = raw_line.strip()
            match state:
                case "next_test":
                    m = test_re.match(line)
                    if m is not None:
                        state = "read_test"
                        current_test = Test(line)
                case "read_test":
                    if len(line) == 0:
                        state = "next_test"
                        if current_test is not None:
                            test_list.append(current_test)
                    else:
                        next_step = parse_step(line)
                        if current_test is not None:
                            current_test.append_step(next_step)
    return test_list

def run_tests(test_list: List[Test], output_stream: TextIOWrapper, is_first_test: bool):
    first = is_first_test
    for test in test_list:
        report = test.exec()
        if report is None:
            continue
        passed = report.fails and "FAIL" or "PASS"
        if not first:
            sys.stdout.write("\n")
            output_stream.write("\n")
        else:
            first = False
        sys.stdout.write(f"\n\n{test.name} {passed}ED\n\n")
        output_stream.write(f"{test.name} {passed}\n")
        output_stream.write(f"{report.get_report_text()}\n")
        output_stream.flush()

class OutputFileContext(object):
    def __init__(self, stream: TextIOWrapper, test_list: List[Test]):
        self.stream = stream
        self.tests = test_list

    def __enter__(self):
        return self.stream, self.tests

    def __exit__(self, exc_type, exc_val, exc_tb):
        self.stream.close()

def open_output_file(fname: str, test_list: List[Test]) -> (TextIOWrapper, List[Test]):
    """Returns the file open to append, with the tests lists
    paired down to only those tests that already appear in the output file,
    allowing the resuming of tests."""
    global test_re
    path = Path(fname)
    if path.is_file():
        done_tests: Set[str] = set()
        with open(path, "r") as f:
            for raw_line in f.readlines():
                line = raw_line.strip()
                words = line.split()
                if len(words) > 0:
                    m = test_re.match(words[0])
                    if m:
                        done_tests.add(words[0])
        remaining_tests = [x for x in test_list if x.name not in done_tests]
    else:
        remaining_tests = test_list
    return OutputFileContext(open(path, "at"), remaining_tests)


if __name__ == "__main__":
    tests = from_file(sys.argv[1])
    with open_output_file(sys.argv[2], tests) as (f, rt):
        run_tests(rt, f, len(rt) == len(tests))
