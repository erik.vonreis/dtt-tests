#!/usr/bin/python3
import sys
from typing import Protocol, Optional, List
from abc import abstractmethod

class Step(Protocol):
    raw_text: str = ""
    @abstractmethod
    def exec(self) -> Optional['Report']:
        raise NotImplementedError

class Action(Step):
    def __init__(self, text: str):
        self.text = text
        self.raw_text = text
        
    def exec(self) -> None:
        sys.stdout.write(self.text + "     <press a key>")
        sys.stdout.flush()
        sys.stdin.read(1)
        sys.stdout.write("\n")

class Notice(Step):
    def __init__(self, text: str):
        self.text = text
        self.raw_text = f"! {self.text}"

    def exec(self) -> None:
        sys.stdout.write(f"NOTICE: {self.text}\n")

class Report(object):
    def __init__(self, report_line: str, fails = False):
        self.report_text = report_line
        self.fails = fails

    def get_report_text(self) -> str:
        return self.report_text

class YesNoQuestion(Step):
    def __init__(self, question: str, correct_yes = True):
        self.question = question
        self.raw_text = f"? {self.question}"
        self.correct_yes = correct_yes

    def exec(self) -> Report:
        while True:
            sys.stdout.write(self.question + " [y/n]")
            sys.stdout.flush()
            answer = sys.stdin.read(1).strip()
            sys.stdout.write("\n")
            if len(answer) > 0 and answer[0] in "ynYN":
                break
            sys.stdout.write("Answer must be 'y' or 'n'\n")
        if len(answer) < 1 or answer[0] not in "yYnN":
            raise Exception(f"Bad answer {answer}")
        if answer[0] in "yY":
            correct = self.correct_yes
        else:
            correct = not self.correct_yes
        passed = correct and "PASS" or "FAIL"
        return Report(f"{self.question} ?  {answer} {passed}", not correct)

class Test(Step):
    def __init__(self, name):
        self.name = name
        self.steps: List[Step] = []

    @property
    def number(self):
        return int(self.name.split('-')[1])

    def append_step(self, step: Step):
        self.steps.append(step)

    def exec(self) -> Optional[Report]:
        if self.skip():
            return None
        sys.stdout.write(f"Starting test {self.name}\n")
        fails = False
        report_lines: List[str] = []
        for step in self.steps:
            report = step.exec()
            if report is not None:
                fails = fails or report.fails
                report_lines.append(report.get_report_text())
        return Report("\n".join(report_lines), fails)

    def skip(self) -> bool:
        """
        Ask if the user wants to skip this particular test
        :return:
        """
        sys.stdout.write(f"Next test is:\n{self.raw_text}\n\n")

        q = YesNoQuestion("Do you want to do this test?")
        r = q.exec()
        return r.fails

    @property
    def  raw_text(self):
        return self.name + "\n" + "\n".join([x.raw_text for x in self.steps])


def parse_step(txt: str) -> Step:
    if txt[0] == '!':
        return Notice(txt[1:].strip())
    elif txt[0] == '?':
        return YesNoQuestion(txt[1:].strip())
    return Action(txt)